# Copyright 2025 Portmod Authors
# Distributed under the terms of the GNU General Public License v3
BASE: Solstheim_Tomb_of_the_Snow_Prince
GRAPHICAL: Solstheim_Graphical_Replacer
inherit:
- common.mw:MW
NAME: Tomb-of-The-Snow-Prince
DESC: Improves the graphical fidelity, design, and gameplay of the Bloodmoon DLC
HOMEPAGE: |-
  https://github.com/Ellnz/Tomb-of-The-Snow-Prince
KEYWORDS: ~openmw
LICENSE: all-rights-reserved
TEXTURE_SIZES: 32 256 1024 2048
SRC_URI: https://github.com/EllNZ/Tomb-of-The-Snow-Prince/archive/P1-${PV}.tar.gz
  -> ${P}.tar.gz

 # Included as a hint for Importmod
IUSE: minimal pfp +grass spiders
RDEPEND:
  base/morrowind[bloodmoon,tribunal]
  pfp? ( base/patch-for-purists )
  !minimal? ( >=landmasses/tamriel-data-8 )
  grass? (
      land-flora/remiros-groundcover[-solstheim]
      !!land-flora/vurts-groundcover[solstheim]
      !!land-flora/ozzy-grass[bloodmoon]
      !!land-flora/aesthesia-groundcover[bloodmoon]
  )

REQUIRED_USE: ?? ( minimal pfp )
S: $P/Tomb-of-The-Snow-Prince-P1-$PV
INSTALL_DIRS:
#### Graphical Replacer ####
- PATH: Solstheim_Graphical_Replacer/010 Solstheim
    - HD Worldspace Graphical Replacer
- PATH: Solstheim_Graphical_Replacer/011 Skyrim-Like
    Trees
- PATH: Solstheim_Graphical_Replacer/012 Remiros'
    Groundcover for TOTSP
  GROUNDCOVER:
  - NAME: Rem-TOTSP.esp
  REQUIRED_USE: grass
- PATH: Solstheim_Graphical_Replacer/013 Grey Solstheim
    Rocks

#### Main Mod ####
# Everything below should be !minimal
- PATH: Solstheim_Tomb_of_the_Snow_Prince/000 Core
  PLUGINS:
  - NAME: Solstheim Tomb of The Snow Prince.esm
  REQUIRED_USE: "!minimal"
- PATH: Solstheim_Tomb_of_the_Snow_Prince/001 Core
    - Interiors With Glow in the Dahrk
  PLUGINS:
  - NAME: GITD_WL_RR_Interiors.esp
  REQUIRED_USE: "!minimal"
- PATH: Solstheim_Tomb_of_the_Snow_Prince/001 Core
    - Interiors Without Glow in the Dahrk
  PLUGINS:
  - NAME: GITD_WL_RR_Interiors.esp
  REQUIRED_USE: "!minimal"
- PATH: Solstheim_Tomb_of_the_Snow_Prince/010 Optional - Tamriel Data Integration
  PLUGINS:
  - NAME: TOTSP TD Content Integration.esp
    REQUIRED_USE: "!spiders"
  - NAME: TOTSP TD Content Integration - Spiders.esp
    REQUIRED_USE: "spiders"
  REQUIRED_USE: "!minimal"
- PATH: Solstheim_Tomb_of_the_Snow_Prince/100 Patch - Patch for Purists
  PLUGINS:
  - NAME: TOTSP_Patch_for_Purists_4.0.2.esp
  REQUIRED_USE: "!minimal pfp"
# - PATH: Solstheim_Tomb_of_the_Snow_Prince/102 Patch - abot's Boats
#  PLUGINS:
#  - NAME: TOTSP_abotBoats.esp
# - PATH: Solstheim_Tomb_of_the_Snow_Prince/199 Patch - Forceful Travel NPC Override
#  PLUGINS:
#  Might be necessary if there are load order issues with other mods
#  - NAME: TOTSP_Forceful_Travel_NPC_Override.ESP
#  REQUIRED_USE: !minimal
- PATH: Solstheim_Tomb_of_the_Snow_Prince/200 Addon
    - Missing Snow Armor Pieces
  PLUGINS:
  - NAME: Missing snow armor.esp
  REQUIRED_USE: "!minimal"
- PATH: Solstheim_Tomb_of_the_Snow_Prince/201 Addon
    - Fierce Wolf Helms
  REQUIRED_USE: "!minimal"
- PATH: Solstheim_Tomb_of_the_Snow_Prince/202 Addon
    - Hide-Like Animal Pelts
  REQUIRED_USE: "!minimal"
