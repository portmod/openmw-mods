# When masking packages for removal, include the following information
#
# 1) Name, email/gitlab username and date of entry
# 2) Reason for masking
# 3) Link to the issue for the removal
# 4) Date of removal
#
# Once the given date has passed, the package and the entry in this file can be removed
# Make sure to also remove references such as dependencies, optional or otherwise


# Version no longer available upstream
=items-misc/wares-2.2.2-r1

# Benjamin Winger <bmw@disroot.org> (2025-02-12)
# Hidden on nexusmods due to being made obsolete by wares ultimate (>=items-misc/wares-e1-2.0)
# No longer supported, but downloads are still available
# Removal on (2025-04-01)
items-misc/wares-for-mods
items-misc/wares-for-npcs
<items-misc/wares-e1-2.0

# This is a placeholder package, it cannot be installed! Consider contributing! https://portmod.gitlab.io/portmod/dev/index.html
common/placeholder
