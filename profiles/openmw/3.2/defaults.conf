ARCH = "openmw"
ACCEPT_LICENSE = "* -@EULA"
PORTMOD_MIRRORS = "https://gitlab.com/portmod-mirrors/openmw/-/raw/master/"
TEXTURE_SIZE = "min"
PROFILE_ONLY_VARIABLES = "PROFILE_ONLY_VARIABLES USE_EXPAND USE_EXPAND_HIDDEN MODULEPATH VFS INSTALL_DEST CONFIGPATH"
INFO_VARS = "ARCH_VERSION ACCEPT_KEYWORDS ACCEPT_LICENSE ARCH CONFIGS PLATFORM PORTMOD_MIRRORS TEXTURE_SIZE USE"
CASE_INSENSITIVE_FILES = True
CACHE_FIELDS = "NEXUS_URL"
LOCAL_FILES = {
    "PLUGINS": r"^.*\.(esp|ESP|esm|ESM|omwaddon|omwgame|omwscripts)$",
    "ARCHIVES": r"^.*\.(bsa|BSA)$",
}
CONFIGPATH = "etc"
CFG_PROTECT = "etc/**/*"
INFO_VARS = "OPENMW_CONFIG_DIR MODULEPATH INSTALL_DEST PYTHONPATH"

USE_EXPAND = "L10N SCREEN_ASPECT MAP PLATFORM"
USE_EXPAND_HIDDEN = "PLATFORM"  # Doesn't need to be visible as it shouldn't need to be modified
L10N = "en"
MAP = "normal specular terrain_normal terrain_specular"
# Path for modules, relative to ROOT
MODULEPATH = "modules"

## Environment Variables
# This is a system thing, so it's not relative to ROOT
# and needs to be an absolute path
PYTHONPATH = f"{ROOT}/lib/python"
# Default destination for InstallDirs (used by common/mw-2)
# Relative to ROOT
INSTALL_DEST = "pkg/{CATEGORY}/{PN}"
PYTHONDONTWRITEBYTECODE = "1"
DOC_DEST = "doc"

if PLATFORM == "linux":
    OPENMW_CONFIG_DIR = "~/.config/openmw:~/.var/app/org.openmw.OpenMW/config/openmw/"
    try:
        if WSL_INTEROP:
            OPENMW_CONFIG_DIR = f"/mnt/c/Users/{USER}/Documents/My Games/OpenMW"
    except:
        pass
elif PLATFORM == "darwin":
    OPENMW_CONFIG_DIR = "~/Library/Preferences/openmw"
elif PLATFORM == "win32":
    OPENMW_CONFIG_DIR = join(PERSONAL, "my games", "openmw")

if PLATFORM == "win32":
    PATH = f"{ROOT}/bin;{PATH}"
    # atlasgen seems to be broken on windows
    USE = "-atlasgen atlas sunrays"
else:
    PATH = f"{ROOT}/bin:{PATH}"
    # Atlasgen is not particularly reliable for project atlas 0.7
    USE = "-atlasgen atlas sunrays"

# Note: ARCH_VERSION will not be set before system packages are installed, so it's necessary to fall back to the default of ARCH (from base) for the initial run
try:
    if ARCH_VERSION:
        ACCEPT_KEYWORDS = f"{ARCH}{{=={ARCH_VERSION}}}"
    else:
        ACCEPT_KEYWORDS = ARCH
except:
    pass
