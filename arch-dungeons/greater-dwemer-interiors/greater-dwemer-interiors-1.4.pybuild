# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from common.mw import MW, File, InstallDir
from common.nexus import NexusMod


class Package(NexusMod, MW):
    NAME = "Greater Dwemer Interiors"
    DESC = "An overhaul of the Dwemer tileset"
    HOMEPAGE = """
        https://www.nexusmods.com/morrowind/mods/45331
        darker? ( https://www.nexusmods.com/morrowind/mods/47127 )
    """
    LICENSE = "all-rights-reserved"
    RDEPEND = """
        base/morrowind[bloodmoon,tribunal]
        tr? ( landmasses/tamriel-data landmasses/tamriel-rebuilt )
    """
    KEYWORDS = "openmw"
    NEXUS_SRC_URI = """
        https://www.nexusmods.com/morrowind/mods/45331?tab=files&file_id=1000031198
        -> DNGDR_Tiles1.4-45331-1-4-1653927383.7z
    """
    SRC_URI = """
        darker? ( https://gitlab.com/portmod-mirrors/openmw/-/raw/master/Darker_Morrowind_Mods-47127-1-0-2-1584666728.7z )
    """
    TEXTURE_SIZES = "1448"
    IUSE = "tr darker"
    INSTALL_DIRS = [
        InstallDir(
            "Data Files",
            S="DNGDR_Tiles1.4-45331-1-4-1653927383",
            PLUGINS=[
                File("DN_Dwemer_Tileset.esm"),
                File("DN_Dwemer_Tileset_TR.esp", REQUIRED_USE="tr"),
            ],
        ),
        InstallDir(".", S="DNGDR_Tiles1.4-45331-1-4-1653927383", DOC=["README.txt"]),
        InstallDir(
            ".",
            S="Darker_Morrowind_Mods-47127-1-0-2-1584666728",
            PLUGINS=[File("Darker DNGDR.esp")],
            REQUIRED_USE="darker",
            WHITELIST=["Darker DNGDR.esp"],
        ),
    ]
