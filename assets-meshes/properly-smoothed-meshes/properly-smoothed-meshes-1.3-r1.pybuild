# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import os

from common.mw import MW, InstallDir
from common.nexus import NexusMod

SOURCE = "Properly_Smoothed_Meshes-46747-1-03-1558647998"


class Package(NexusMod, MW):
    NAME = "Properly Smoothed Meshes"
    DESC = "A graphic replacer covering misc items, furniture, lights, and containers"
    HOMEPAGE = "https://www.nexusmods.com/morrowind/mods/46747"
    NEXUS_URL = "https://www.nexusmods.com/morrowind/mods/46747"
    LICENSE = "all-rights-reserved"  # Contains modified vanilla assets
    TIER = 1
    KEYWORDS = "openmw"
    SRC_URI = f"{SOURCE}.7z"
    IUSE = """
        textures
        flat-tables
        high-poly-candles
        beakers
        inkwell
        barrel
        potted-plant
    """
    DATA_OVERRIDES = "assets-meshes/correct-meshes assets-meshes/rr-better-meshes"
    # Other overrides: Tarius' Better Meshes, Glowing Flames
    RDEPEND = ""
    # !!Qarl's Bowls
    # !!Qarl's Flasks
    TEXTURE_SIZES = "512"
    INSTALL_DIRS = [
        InstallDir("00 Core"),
        InstallDir("01 Optional Textures", REQUIRED_USE="textures"),
        InstallDir("02 Flat Tables", REQUIRED_USE="flat-tables"),
        InstallDir("03 Higher Poly Glowing Candles", REQUIRED_USE="high-poly-candles"),
        InstallDir("05 Beaker, Flasks - clear glass", REQUIRED_USE="beakers"),
        InstallDir("06 Inkwell - cut glass", REQUIRED_USE="inkwell"),
        InstallDir("07 Barrel - no rivets", REQUIRED_USE="barrel"),
        InstallDir("08 Potted Plant - redware", REQUIRED_USE="potted-plant"),
    ]

    def src_prepare(self):
        renamedirs = [
            "05 beaker, flasks - clear glass",
            "06 Inkwell - cut glass",
            "07 Barrel - no rivets",
            "08 Potted Plant - redware",
        ]
        for path in renamedirs:
            for directory in ["Icons", "Meshes", "Textures"]:
                fullpath = os.path.join(path, directory)
                if os.path.exists(fullpath):
                    os.rename(fullpath, os.path.join(path, directory.lower()))
