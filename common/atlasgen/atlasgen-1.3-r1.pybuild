# Copyright 2019-2020 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import os
import re
import shutil
import sys
from typing import List, Tuple

from pybuild import Pybuild2

from common.mw import MW, InstallDir


class Package(Pybuild2):
    NAME = "AtlasGen"
    DESC = "Provides code for parsing and executing atlas texture generators"
    KEYWORDS = "openmw"


def bashconvert(file):
    with open(file, "r") as f:
        lines = f.readlines()

    def convertline(line):
        # Comment out unnecessary commands
        if (
            re.search("^(::|REM|@echo|pause)", line)
            or line.strip() == "if not defined run_without_pause pause"
        ):
            line = "# " + line

        line = line.strip()

        # Arithmetic
        line = re.sub(r"set /a (.*)", r"((\1))", line, flags=re.IGNORECASE)

        # Assigment
        line = re.sub(r"set (\w*)=", r"\1=", line, flags=re.IGNORECASE)

        # Seems like an escape on arithmetic. Bash doesn't try to turn text into arithmetic
        line = re.sub(r"-resize (\w+)\%\%x(\w+)\%\%", r"-resize \1x\2", line)
        # Remove other /. values.
        # For most arithmetic/boolean expressions bash inferences the type
        line = re.sub(" /. ", " ", line)
        line = re.sub(r"(?<!-format )\%\%", r"$", line)
        line = re.sub(r"-format \%\%", r"-format \%", line)
        line = re.sub(r"\%(\w+)\%", r"${\1}", line)
        # Needs -f as del ignores nonexistant files
        line = re.sub("^del ", "rm -f ", line, flags=re.IGNORECASE)
        line = re.sub("'(.*)'", r"`\1`", line)
        line = re.sub("^cd..$", "cd ..", line)

        line = re.sub(
            r"^if not exist (.*) mkdir (.*)",
            r"if [[ ! -r \1 ]]; then mkdir \2; fi",
            line,
            flags=re.IGNORECASE,
        )
        match = re.search(
            r"^for (?P<A>.*) in \((?P<B>.*)\) do (?P<C>.*)", line, flags=re.IGNORECASE
        )
        if match:
            var = match.group("A")
            var = var.lstrip("%$")
            iterable = match.group("B")
            iterable.replace("'", "`")
            command = match.group("C")
            if iterable.startswith("1,1,"):
                iterable = iterable.replace("1,1,", "")
                iterable = f"`seq {iterable}`"
            line = f"for {var} in {iterable}; do {command}; done"

        return line

    def convert_lines(lines):
        output = []
        while lines:
            line = lines[0]

            # Single line
            line = convertline(line)

            # Multi line
            match = re.match(r"if (.*) (\w*) (.*) \(", line, flags=re.IGNORECASE)
            if match:
                condition = match.group(1)
                ops = {"GEQ": ">=", "NEQ": "!=", "LEQ": "<=", "EQ": "=="}
                op = ops[match.group(2)]
                value = match.group(3)
                command = []
                del lines[0]
                line = lines[0]
                while not re.search(r"^\)$", line):
                    command.append(line)
                    del lines[0]
                    line = lines[0]
                line = ""
                output.append(f"if (( {condition} {op} {value} )); then")
                # Treat inner as its own thing
                output.extend(["    " + line for line in convert_lines(command)])
                output.append("fi")

            line = line.strip()

            output.append(line)
            del lines[0]
        return output

    return "\n".join(convert_lines(lines))


class AtlasGen(MW):
    def __init__(self):
        self.IUSE = self.IUSE | {"atlasgen", "map_normal", "map_specular"}
        self.IUSE_EFFECTIVE = self.IUSE_EFFECTIVE | {
            "atlasgen",
            "map_normal",
            "map_specular",
        }
        self.INSTALL_DIRS.append(
            InstallDir("atlases", S="atlasgen", REQUIRED_USE="atlasgen")
        )
        self.DEPEND += " modules/configtool"

    def find_files_for_suffix(self, scriptpath, suffix) -> List[Tuple[str, str]]:
        from configtool.openmw import find_file

        # Copy files that will be included in the atlas
        files = set()
        absolute_files = []
        with open(scriptpath, "r") as file:
            for line in file.readlines():
                for ddsfile in re.findall(
                    r'([\\/\w]+\.(dds|\*)|"[\\/\w\s]+\.(dds|\*)")',
                    line,
                    flags=re.IGNORECASE,
                ):
                    # FIXME: Generators seem now to be designed for any image file, not just dds
                    stripped = ddsfile[0].strip().strip('"').replace(".*", ".dds")
                    if "atl" not in os.path.dirname(stripped).lower():
                        files.add(stripped)

        for file in files:
            file = file.strip()
            name, ext = os.path.splitext(file)
            path = os.path.join("textures", name + suffix + ext)
            self.REBUILD_FILES.append(path)
            src = find_file(path)
            if src:
                absolute_files.append((src, path))
            else:
                raise FileNotFoundError(f"No file found to satisfy textures/{file}")
        return absolute_files

    def generate_atlases_oftype(self, typ, suffix=""):
        """
        Generates a set of atlases for the given suffix

        Suffixes are added to the fileset when searching,
        and included in the atlases at the end
        """
        os.makedirs(os.path.join(self.T, "textures"), exist_ok=True)
        self.REBUILD_FILES = []

        for install_dir, scriptfile in self.get_files("ATLASGEN"):
            scriptpath = os.path.join(
                self.WORKDIR, install_dir.S or self.S, install_dir.PATH, scriptfile.NAME
            )

            files = self.find_files_for_suffix(scriptpath, suffix)

            # Copies into "textures", so we need to be in that directory's parent
            os.chdir(self.T)
            for src, filename in files:
                print(f"Copying {src} to temporary directory")
                shutil.copyfile(src, filename)

            os.chdir(os.path.join(self.T, "textures"))
            if sys.platform == "win32":
                print(f"Running {os.path.basename(scriptpath)}...")
                self.execute(f'"{scriptpath}"')
            else:
                print(f"Converting {os.path.basename(scriptpath)} to bash...")
                script = bashconvert(scriptpath)

                name, ext = os.path.splitext(scriptfile.NAME)
                with open(f"{name}.sh", "w") as file:
                    file.write(script)
                print("Running converted script...")
                self.execute(f"bash {name}.sh")

            os.chdir(self.T)
            # Remove the original texture files,
            # we only care about installing the atlases
            for _, file in files:
                try:
                    os.remove(file.strip())
                except FileNotFoundError:
                    continue

        # Install atlases
        dest = os.path.join(self.WORKDIR, "atlasgen", "atlases")
        os.makedirs(dest, exist_ok=True)
        for root, dirs, files in os.walk("."):
            for name in dirs:
                os.makedirs(os.path.join(dest, root, name), exist_ok=True)
            for file in files:
                name, ext = os.path.splitext(file)
                shutil.move(
                    os.path.join(root, file),
                    os.path.join(dest, root, name + suffix + ext),
                )

    def check_required_files(self):
        if "atlasgen" in self.USE:
            for install_dir, scriptfile in self.get_files("ATLASGEN"):
                scriptpath = os.path.join(
                    self.WORKDIR,
                    install_dir.S or self.S,
                    install_dir.PATH,
                    scriptfile.NAME,
                )
                self.find_files_for_suffix(scriptpath, "")
                if "map_normal" in self.USE:
                    self.find_files_for_suffix(scriptpath, "_n")
                if "map_specular" in self.USE:
                    self.find_files_for_suffix(scriptpath, "_spec")

    def src_prepare(self):
        super().src_prepare()
        self.check_required_files()
        if "atlasgen" in self.USE:
            try:
                self.generate_atlases_oftype("Textures")
                if "map_normal" in self.USE:
                    self.generate_atlases_oftype("Normal Maps", "_n")
                if "map_specular" in self.USE:
                    self.generate_atlases_oftype("Specular Maps", "_spec")
            except Exception as e:
                print("An exception has been raised!")
                print("The project atlas wiki page includes details on common errors")
                print(
                    "https://gitlab.com/portmod/portmod/-/wikis/openmw/pkg/project-atlas"
                )
                raise e
