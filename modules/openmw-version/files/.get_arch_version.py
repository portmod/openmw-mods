#!/usr/bin/env python3

# Copyright 2019-2020 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

"""Finds openmw's version"""

import os
import sys
import shutil


def get_key(reg, subkey):
    import winreg

    try:
        return winreg.OpenKey(
            reg, subkey, access=winreg.KEY_READ | winreg.KEY_WOW64_64KEY
        )
    except FileNotFoundError:
        try:
            return winreg.OpenKey(
                reg, subkey, access=winreg.KEY_READ | winreg.KEY_WOW64_64KEY
            )
        except FileNotFoundError:
            return None


def get_windows_install_path():
    import winreg

    key = winreg.HKEY_LOCAL_MACHINE
    subkey = "Software\\Wow6432Node\\OpenMW.org"
    with winreg.ConnectRegistry(None, key) as reg:
        rawkey = get_key(reg, subkey)
        if rawkey is None:
            return None

    subkeys = {}
    i = 0
    while True:
        try:
            subsubkey = winreg.EnumKey(rawkey, i)
            subkeys[subsubkey] = winreg.QueryValue(key, subkey + os.sep + subsubkey)
            i += 1
        except WindowsError:
            break
    if len(subkeys) == 1:
        return next(iter(subkeys.values()))
    else:
        raise Exception(
            "Multiple OpenMW installations were found. "
            "Please indicate which one you are using by setting the OPENMW_VERSION_FILE "
            "environment variable to the path of the resources/version file within the OpenMW installation:"
            + os.linesep
            + os.linesep.join(list(subkeys.keys()))
        )


version_file_paths = []
USER_VERSION_FILE = os.getenv("OPENMW_VERSION_FILE")
# If user has defined the OPENMW_VERSION_FILE, skip other checks to speed things up
if USER_VERSION_FILE:
    version_file_paths.append(USER_VERSION_FILE)
elif sys.platform == "linux":
    version_file_paths.extend(
        [
            "/usr/share/openmw/resources/version",
            "/usr/share/games/openmw/resources/version",
            "/etc/openmw/version",
        ]
    )

    XDG_DATA_HOME = os.getenv("XDG_DATA_HOME")

    # Following the spec: https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html
    # XDG_DATA_HOME should fall back to $HOME/.local/share if it is not set
    if XDG_DATA_HOME is None:
        HOME = os.getenv("HOME")
        XDG_DATA_HOME = f"{HOME}/.local/share"

    version_file_paths.extend(
        [
            f"{XDG_DATA_HOME}/flatpak/app/org.openmw.OpenMW/current/active/files/etc/openmw/version",
            f"{XDG_DATA_HOME}/flatpak/app/org.openmw.OpenMW/current/active/files/share/games/openmw/resources/version",
        ]
    )

    # For Nix specifically
    linux_executable = shutil.which("openmw")
    if linux_executable:
        version_file_paths.append(
            os.path.join(os.path.dirname(os.path.dirname(linux_executable)), "share/games/openmw/resources/version")
        )

elif sys.platform == "darwin":
    version_file_paths.append(
        "/Applications/OpenMW.app/Contents/Resources/resources/version"
    )
elif sys.platform == "win32":
    windows_install = get_windows_install_path()
    if windows_install:
        version_file_paths.append(os.path.join(windows_install, "resources", "version"))

found = False
for path in version_file_paths:
    if path is not None and os.path.exists(path):
        found = True
        with open(path, encoding="utf-8") as file:
            version = file.readline().strip()
            print(version, end="")
        break

if not found:
    raise FileNotFoundError(
        f"Unable to find OpenMW's version file.{os.pathsep}"
        "Note that you can specify a custom path for OpenMW's version file "
        '(which should be in the "resources" directory) via the '
        "OPENMW_VERSION_FILE environment variable (which should NOT be set in "
        "portmod.conf, since version detection is loaded first)"
    )
